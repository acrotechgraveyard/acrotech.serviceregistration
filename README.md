﻿# README #

This portable library provides an interface that compliments the CommonServiceLocator framework (IServiceLocator).

The concept behind this library is to provide a common interface to basic registration functions of all IoC frameworks. This means that for applications that do not require advanced IoC functionality, this library provides an abstract interface-based access to the IoC container without requiring a direct reference to the IoC library itself. This is a benefit for any small apps that don't want to decide immediately on an IoC framework, but still want to code against one (i.e., have the ability to change the IoC framework down the road without requiring any code changes).

This library was modelled using Autofac, but should be adaptable to use almost any IoC framework.

### What is this repository for? ###

* Anyone who is writing (cross platform) applications and wants to use inversion of control but doesn't want to be stuck with the first choice of IoC framework.

### How do I get set up? ###

0. Include this library (portable side)
0. Include an adapter library (non-portable side, for the type of IoC framework you want to use)
0. Create the IoC registrator object and pass it down to the portable side (as an interface)

### Who do I talk to? ###

* Contact me for suggestions, improvements, or bugs

### Changelog ###

#### 1.0.0.0 ####

* Initial Release
